package aaronraymond.cs4743assignment;

import java.time.LocalDate;

public class Person {

  private int id;
  private String firstName;
  private String lastName;
  private LocalDate dateOfBirth;
  private int age;

  public Person(
      int newId, String newFirstName, String newLastName, LocalDate newDateOfBirth, int newAge) {
    this.id = newId;
    this.firstName = newFirstName;
    this.lastName = newLastName;
    this.dateOfBirth = newDateOfBirth;
    this.age = newAge;
  }

  // Getters
  public int getId() {
    return id;
  }

  public String getFirstName() {
    return firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public LocalDate getDateOfBirth() {
    return dateOfBirth;
  }

  public int getAge() {
    return age;
  }

  // Setters
  public void setId(int newId) {
    this.id = newId;
  }

  public void setFirstName(String newFirstName) {
    this.firstName = newFirstName;
  }

  public void setLastName(String newLastName) {
    this.lastName = newLastName;
  }

  public void setDateOfBirth(LocalDate newDateOfBirth) {
    this.dateOfBirth = newDateOfBirth;
  }

  public void setAge(int newAge) {
    this.age = newAge;
  }

  @Override
  public String toString() {
    return getFirstName() + " " + getLastName();
  }
}
