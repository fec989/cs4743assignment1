package aaronraymond.cs4743assignment;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

public class SessionGateway {
  private static final String WS_URL = "http://localhost:8080";
  private static String token = null;

  public static String getResponseAsString(CloseableHttpResponse response) throws IOException {
    HttpEntity entity = response.getEntity();
    String strResponse = EntityUtils.toString(entity, StandardCharsets.UTF_8);
    EntityUtils.consume(entity);
    return strResponse;
  }

  public static String getToken() {
    return token;
  }

  public SessionGateway(String userName, String password) {
    ArrayList<Person> people = new ArrayList<Person>();
    CloseableHttpResponse response = null;
    CloseableHttpClient httpclient = null;

    try {
      httpclient = HttpClients.createDefault();
      JSONObject credentials = new JSONObject();
      credentials.put("username", userName);
      credentials.put("password", password);
      String credentialsString = credentials.toString();
      StringEntity reqEntity = new StringEntity(credentialsString);
      HttpPost loginRequest = new HttpPost(WS_URL + "/login");
      loginRequest.setEntity(reqEntity);
      loginRequest.setHeader("Accept", "application/json");
      loginRequest.setHeader("Content-type", "application/json");
      response = httpclient.execute(loginRequest);

      if (response.getStatusLine().getStatusCode() != 200) {
        httpclient.close();
        return;
      }

      String responseString = getResponseAsString(response);

      try {
        JSONObject responseJSON = new JSONObject(responseString);
        token = responseJSON.getString("session_id");
      } catch (Exception e) {
        httpclient.close();
        return;
      }

    } catch (Exception e) {
      e.printStackTrace();
    } finally {
      try {
        response.close();
        httpclient.close();
      } catch (IOException e) {
      }
    }
  }
}
