package aaronraymond.cs4743assignment;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

public class PersonGateway {
  private static final String WS_URL = "http://localhost:8080";
  private static ArrayList<Person> people = new ArrayList<Person>();
  private static CloseableHttpResponse response = null;
  private static CloseableHttpClient httpclient = null;

  public PersonGateway() {}

  public static String getResponseAsString(CloseableHttpResponse response) throws IOException {
    HttpEntity entity = response.getEntity();
    String strResponse = EntityUtils.toString(entity, StandardCharsets.UTF_8);
    EntityUtils.consume(entity);
    return strResponse;
  }

  public static ArrayList<Person> fetchPeople(String token) {
    try {
      httpclient = HttpClients.createDefault();
      HttpGet request = new HttpGet(WS_URL + "/people");
      request.setHeader("Authorization", token);
      response = httpclient.execute(request);

      if (response.getStatusLine().getStatusCode() != 200) {
        httpclient.close();
        return null;
      }

      String responseString = getResponseAsString(response);
      JSONArray jsonArray = new JSONArray(responseString);
      DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("MM-dd-yyyy");
      for (int i = 0; i < jsonArray.length(); i++) {
        JSONObject personObject = jsonArray.getJSONObject(i);
        Person person =
            new Person(
                personObject.getInt("id"),
                personObject.getString("firstName"),
                personObject.getString("lastName"),
                LocalDate.parse(personObject.getString("dateOfBirth"), dateTimeFormatter),
                personObject.getInt("age"));
        people.add(person);
      }

      try {

        return people;
      } catch (Exception e) {
        httpclient.close();
        return null;
      }

    } catch (Exception e) {
      e.printStackTrace();
    } finally {
      try {
        response.close();
        httpclient.close();
      } catch (IOException e) {
      }
    }
    return null;
  }

  public static void updatePeople(String token) {
    try {

      JSONObject updatePeopleObject = new JSONObject();
      updatePeopleObject.put("firstName", "bobby");

      StringEntity reqEntity = new StringEntity(updatePeopleObject.toString());

      httpclient = HttpClients.createDefault();
      HttpPut request = new HttpPut(WS_URL + "/people/1");
      request.setEntity(reqEntity);
      request.setHeader("Accept", "application/json");
      request.setHeader("Content-type", "application/json");
      request.setHeader("Authorization", token);

      response = httpclient.execute(request);

      if (response.getStatusLine().getStatusCode() != 200) {
        httpclient.close();
        return;
      }

      try {
        return;
      } catch (Exception e) {
        httpclient.close();
        return;
      }

    } catch (Exception e) {
      e.printStackTrace();
    } finally {
      try {
        response.close();
        httpclient.close();
      } catch (IOException e) {
      }
    }
    return;
  }

  public static void deletePeople(String token) {
    try {
      httpclient = HttpClients.createDefault();
      HttpDelete request = new HttpDelete(WS_URL + "/people/1");
      request.setHeader("Authorization", token);
      response = httpclient.execute(request);

      if (response.getStatusLine().getStatusCode() != 200) {
        httpclient.close();
        return;
      }

      try {
        return;
      } catch (Exception e) {
        httpclient.close();
        return;
      }

    } catch (Exception e) {
      e.printStackTrace();
    } finally {
      try {
        response.close();
        httpclient.close();
      } catch (IOException e) {
      }
    }
    return;
  }

  public static void insertPeople(String token) {
    try {

      JSONObject newPerson = new JSONObject();
      newPerson.put("firstName", "Lou");
      newPerson.put("lastName", "Smith");
      newPerson.put("dateOfBirth", "1990-01-01");

      httpclient = HttpClients.createDefault();
      HttpPost request = new HttpPost(WS_URL + "/people");
      StringEntity reqEntity = new StringEntity(newPerson.toString());
      request.setEntity(reqEntity);
      request.setHeader("Accept", "application/json");
      request.setHeader("Content-type", "application/json");

      request.setHeader("Authorization", token);
      response = httpclient.execute(request);

      if (response.getStatusLine().getStatusCode() != 200) {
        httpclient.close();
        return;
      }

      try {
        return;
      } catch (Exception e) {
        httpclient.close();
        return;
      }

    } catch (Exception e) {
      e.printStackTrace();
    } finally {
      try {
        response.close();
        httpclient.close();
      } catch (IOException e) {
      }
    }
    return;
  }
}
