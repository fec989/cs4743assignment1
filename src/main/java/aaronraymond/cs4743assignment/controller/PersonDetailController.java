package aaronraymond.cs4743assignment;

import java.net.URL;
import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class PersonDetailController implements Initializable {
  private static Logger logger = LogManager.getLogger();

  @FXML private TextField id;
  @FXML private TextField firstName;
  @FXML private TextField lastName;
  @FXML private TextField dateOfBirth;
  @FXML private TextField age;
  @FXML private Button saveButton;
  @FXML private Button cancelButton;

  private Person person;

  public PersonDetailController(Person person) {
    this.person = person;
  }

  @FXML
  void savePerson(ActionEvent event) {
    if (firstName.getText().length() == 0 || firstName.getText().length() > 100) {
      logger.info("Invalid First Name\n");
      return;
    } else {
      logger.info("First Name Success\n");
    }
    if (lastName.getText().length() == 0 || lastName.getText().length() > 100) {
      logger.info("Invalid Last Name\n");
      return;
    } else {
      logger.info("Last Name Success\n");
    }
    DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("MM-dd-yyyy");
    LocalDate userDate = LocalDate.parse(dateOfBirth.getText(), dateTimeFormatter);
    LocalDate now = LocalDate.now();
    String currentTime = now.format(dateTimeFormatter);
    LocalDate currentDate = LocalDate.parse(currentTime, dateTimeFormatter);
    Calendar today = Calendar.getInstance();
    Period age = Period.between(userDate, now).normalized();
    int years = age.getYears();

    if (userDate.compareTo(currentDate) >= 0) {
      logger.info("Invalid Date of Birth\n");
      return;
    } else {
      logger.info("User age is " + years + "\n");
      logger.info("Date of Birth Success\n");
    }
    PersonGateway personGateway = new PersonGateway();
    if (person == null) {
      personGateway.insertPeople(MainController.getInstance().getToken());
      logger.info("CREATING " + firstName.getText() + " " + lastName.getText());
    } else {

      personGateway.updatePeople(MainController.getInstance().getToken());
      logger.info("UPDATING " + person.toString());
    }

    MainController.getInstance().switchView(ViewType.PersonListView);
  }

  @FXML
  void cancelPerson(ActionEvent event) {
    MainController.getInstance().switchView(ViewType.PersonListView);
  }

  @FXML
  void goToListView(ActionEvent event) {
    MainController.getInstance().switchView(ViewType.PersonListView);
  }

  @Override
  public void initialize(URL arg0, ResourceBundle arg1) {
    id.setDisable(true);
    age.setDisable(true);
    if (person != null) {
      id.setText("" + person.getId());
      firstName.setText(person.getFirstName());
      lastName.setText(person.getLastName());
      dateOfBirth.setText("" + person.getDateOfBirth());
      age.setText("" + person.getAge());
    }
  }
}
