package aaronraymond.cs4743assignment;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.layout.BorderPane;

public class MainController implements Initializable {
  private static MainController instance = null;
  private static String token = null;

  @FXML private BorderPane rootPane;

  private MainController() {}

  public static MainController getInstance() {
    if (instance == null) {
      instance = new MainController();
    }
    return instance;
  }

  public void setToken(String token) {
    this.token = token;
  }

  public String getToken() {
    return token;
  }

  public void switchView(ViewType viewType) {
    FXMLLoader loader = null;
    try {
      switch (viewType) {
        case LoginView:
          loader = new FXMLLoader(MainController.class.getResource("/LoginView.fxml"));
          loader.setController(new LoginController());
          break;

        case PersonDetailView:
          loader = new FXMLLoader(MainController.class.getResource("/PersonDetailView.fxml"));
          loader.setController(new PersonDetailController(PersonParameters.getPersonParm()));
          break;

        case PersonListView:
          loader = new FXMLLoader(MainController.class.getResource("/PersonListView.fxml"));
          loader.setController(new PersonListController());
          break;
        default:
          break;
      }
      Parent rootNode = loader.load();
      rootPane.setCenter(rootNode);

    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  @Override
  public void initialize(URL arg0, ResourceBundle arg1) {
    switchView(ViewType.LoginView);
  }
}
