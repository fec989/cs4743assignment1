package aaronraymond.cs4743assignment;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.input.MouseEvent;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class PersonListController implements Initializable {
  @FXML private Button addPersonButton;
  @FXML private Button deletePersonButton;
  @FXML private ListView<Person> personListView;

  private ObservableList<Person> people;
  private Person personSelected;
  private static Logger logger = LogManager.getLogger();

  public PersonListController() {
    people = FXCollections.observableArrayList();
    PersonGateway personGateway = new PersonGateway();
    ArrayList<Person> fetchedPeople = new ArrayList<Person>();
    fetchedPeople = personGateway.fetchPeople(MainController.getInstance().getToken());
    for (Person person : fetchedPeople) {
      people.add(person);
    }

    // people.add(new Person(0, "FirstN", "FirstL", LocalDate.of(1990, 1, 7), 0));
    // people.add(new Person(1, "SecondN", "SecondL", LocalDate.of(1978, 2, 6), 1));
    // people.add(new Person(2, "ThirdN", "ThirdL", LocalDate.of(1222, 3, 5), 2));
  }

  @FXML
  public void handleMouseClick(MouseEvent arg0) {
    personSelected = personListView.getSelectionModel().getSelectedItem();
  }

  @FXML
  void deletePerson(ActionEvent event) {
    if (personSelected != null) {
      logger.info("DELETING " + personSelected.toString());
    }

    PersonGateway personGateway = new PersonGateway();
    personGateway.deletePeople(MainController.getInstance().getToken());
  }

  @FXML
  void goToAddView(ActionEvent event) {
    if (personSelected == null) {
      PersonParameters.setPersonParm(null);
    } else {
      PersonParameters.setPersonParm(personSelected);
    }
    MainController.getInstance().switchView(ViewType.PersonDetailView);
  }

  @Override
  public void initialize(URL arg0, ResourceBundle arg1) {
    personListView.setItems(people);
  }
}
