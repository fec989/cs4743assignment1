package aaronraymond.cs4743assignment;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class LoginController {
  private static Logger logger = LogManager.getLogger();

  @FXML private TextField userName;
  @FXML private PasswordField password;
  @FXML private Button loginButton;

  public LoginController() {}

  @FXML
  void loginAction(ActionEvent event) {
    // userName :"ragnar";
    // password : "flapjacks";
    // userName.setText("ragnar");
    // password.setText("flapjacks");
    SessionGateway sessionGateway = new SessionGateway(userName.getText(), password.getText());
    String token = sessionGateway.getToken();
    if (token != null) {
      logger.info(userName.getText() + " LOGGED IN");
      MainController.getInstance().setToken(token);
      MainController.getInstance().switchView(ViewType.PersonListView);
    }
  }
}
