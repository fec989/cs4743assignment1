package aaronraymond.cs4743assignment;

public enum ViewType {
  LoginView,
  PersonListView,
  PersonDetailView
}
