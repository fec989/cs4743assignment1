package aaronraymond.cs4743assignment;

// CS 4743 ASsignment 2 Aaron Nguyen (jze960) Raymond Alvarez (fec989)

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {

  public static void main(String[] args) {
    launch(args);
  }

  @Override
  public void start(Stage stage) throws Exception {
    FXMLLoader loader = new FXMLLoader(this.getClass().getResource("/MainView.fxml"));
    loader.setController(MainController.getInstance());
    Parent rootNode = loader.load();
    Scene scene = new Scene(rootNode);
    stage.setScene(scene);
    stage.setTitle("CS4743 Fall 2020");
    stage.show();
  }
}
